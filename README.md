# Project Description:- 

* Portal zur Verwaltung von Raumbuchung für Besprechungsräume
* Benutzer müssen sich einloggen um die Seite zu benutzen. 
    * Wird eine Unterseite aufgerufen, ohne eingeloggt zu sein, wird man auf die Loginseite geleitet
    * Loginstatus wird über den Context verwaltet
    * Benutzer sind mit Passwort hardcoded im Frontend 
 * 2 Rollen
    * Admin:
        * Kann Räume anlegen und verwalten 
        * Sieht alle Buchungen zu einem Raum
    * User:
        * Sieht seine Buchungen, kann Buchung wieder stornieren
        * Kann Räume stundenweise buchen
 * Die Seite ist vollständig Mobil nutzbar
 * Die Seite kann auf Deutsch und English benutzt werden
 * Ein Raum kann nicht doppelt gebucht werden
 * Seite ist angelehnt an Syngenio CI

 * Technische Anforderungen:

 * React Frontend
    * Routing mit React-Router
    *  Stateverwaltung mit Redux oder Context
    * Rect-intl zur Internationalisierung
 * Spring Backend
    * Restschnittstellen
    * Datenspeicherung in einer H2 DB

### `.\gradlew.bat bulid`

This is build and gets all the gradle dependencies in  project 

### `.\gradlew.bat bootrun`

This start the server. To access H2 database :- Open [http://localhost:8080] to view it in the browser.


### `H2 Database`

Change "spring.jpa.hibernate.ddl-auto=create" in application.properties when creating the H2 database for the first time. Change "spring.jpa.hibernate.ddl-auto=update" in application.properties after building the database so that new entries in the database are updated.


