package com.example.WebApp.Backend.controller;
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;  
import org.springframework.web.bind.annotation.GetMapping;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.PostMapping;  
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.WebApp.Backend.modal.MeetingRoom;
import com.example.WebApp.Backend.service.MeetingRoomService;

//creating RestController  
@RestController

public class MeetingRoomController {
    
    //autowired the MeetingRoomService class  
    @Autowired  
    MeetingRoomService meetingRoomService;
    
    //creating a get mapping that retrieves all the meetings detail from the database
    @GetMapping("/meetings")
    @ResponseBody
    private List<MeetingRoom> getAllMeetingList(@RequestParam String room) {
        return meetingRoomService.getAllMeetingList(room);
    }

    // creating a get mapping that retrieves all the meetings detail using type from
    // the
    // database
    @GetMapping("/meetingByRole")
    @ResponseBody
    private List<MeetingRoom> getMeetingByType(@RequestParam String role, @RequestParam String room) {
        return meetingRoomService.getMeetingByType(role, room);
    }
    
    //creating a get mapping that retrieves the detail of a specific meeting by id  
    @GetMapping("/meeting/{id}")
    //TODO in einer API sollte man niemals die Datenbank DTOs nach ausen geben. Damit verräts du unter anderem deine Datenstruktur in der DB
    //Für die APIs am besten immer eigene DTOs machen und mappen
    private MeetingRoom getMeetingById(@PathVariable("id") int id)   
    {
        return meetingRoomService.getMeetingById(id);
    }
    
    //creating a delete mapping that deletes a specific meeting by id  
    @DeleteMapping("/meeting/{id}")  
    private void deleteMeeting(@PathVariable("id") int id)   
    {
        meetingRoomService.delete(id);
    }
    
    //creating post mapping that post the meeting detail in the database  
    @PostMapping("/meeting")  
    //TODO in einer API sollte man niemals die Datenbank DTOs nach ausen geben. Damit verräts du unter anderem deine Datenstruktur in der DB
    //Für die APIs am besten immer eigene DTOs machen und mappen
    private int saveMeeting(@RequestBody MeetingRoom meetingRoom)   
    {  
        meetingRoomService.saveOrUpdate(meetingRoom);  
        return meetingRoom.getId();  
    }  
}
