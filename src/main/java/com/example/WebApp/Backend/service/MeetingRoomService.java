package com.example.WebApp.Backend.service;
import java.util.ArrayList;  
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;  
import com.example.WebApp.Backend.modal.MeetingRoom;
import com.example.WebApp.Backend.repository.MeetingRoomRepository;

@Service

public class MeetingRoomService{  
    @Autowired  
    MeetingRoomRepository meetingRoomRepository;  
    //getting all meetings records  

    public List<MeetingRoom> getAllMeetingList(String room) 
    {
        List<MeetingRoom> meetingRoom = new ArrayList<MeetingRoom>();
        meetingRoomRepository.findAll().forEach(meeting -> {
            if (meeting.getRoom().equals(room)) {
                meetingRoom.add(meeting);
            }
        });
        return meetingRoom;
    }
    
    //getting a specific record  by Id
    public MeetingRoom getMeetingById(int id)   
    {
        return meetingRoomRepository.findById(id).get();
    }
    
    // getting a specific record by type
    public List<MeetingRoom> getMeetingByType(String role, String room) {
        List<MeetingRoom> meetingRoom = new ArrayList<MeetingRoom>();
        meetingRoomRepository.findAll().forEach(meeting -> {
            if (meeting.getRole().equals(role) && meeting.getRoom().equals(room)) {
                meetingRoom.add(meeting);   
            }
        });
        return meetingRoom;
    }

    public void saveOrUpdate(MeetingRoom meeting)   
    {
        meetingRoomRepository.save(meeting);
    }
    
    //deleting a specific record  by ID
    public void delete(int id)   
    {
        meetingRoomRepository.deleteById(id);
    }
    
}