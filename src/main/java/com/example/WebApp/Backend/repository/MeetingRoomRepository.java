package com.example.WebApp.Backend.repository;
import org.springframework.data.repository.CrudRepository;
import com.example.WebApp.Backend.modal.MeetingRoom;

public interface MeetingRoomRepository extends CrudRepository<MeetingRoom, Integer> {}
